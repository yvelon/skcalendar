struct calendar {
    char *name;
    char *path;
};

struct appearence {
    unsigned int x_border;
    unsigned int y_border;
    unsigned int size_agenda; /* Agenda will take 1/size_agenda of screen size */
    unsigned int size_top;
    unsigned int size_bottom;
    unsigned int view_day_margins;
};

struct calendar calendars[] = {
    [0].name = "Work",
    [0].path = "/tmp/cal/work/",
    [1].name = "Personal",
    [1].path = "/tmp/cal/personal/",
};

const unsigned int min_year = 2000;
const unsigned int max_year = 2050;

/* Colors */

/* Store the colors we need in an array
 * Later we can redifine each color by using RGB components */
enum { cal_1 = 100, cal_2, cal_3 }; /* Add colors as needed */
enum { c_1_1, c_1_2, c_2_1, c_2_2, c_3_1, c_3_2 };

const unsigned int colors[][2] = {
    /*           fg,     bg */
    [cal_1]    = {c_1_1, c_1_2},
    [cal_2]    = {c_2_1, c_2_2},
    [cal_3]    = {c_3_1, c_3_1},
};
const unsigned int n_colors = sizeof(colors)/sizeof(colors[0]);

const unsigned int rgb_colors[][3] = {
    /*           R    G    B     */
    [c_1_1] = {51 ,  92, 103},
    [c_1_2] = {255, 243, 176},
    [c_2_1] = {231, 236, 239},
    [c_2_2] = {39,   76, 119},
    [c_3_1] = {169, 146, 125},
    [c_3_2] = {73,   17,  28},
};

struct appearence app = {
    .x_border = 1,
    .y_border = 0,
    .size_agenda = 5,
    .size_top = 3, /* Minimum value should be 2. Use 3 for additional space between the month and the calendar */
    .view_day_margins = 5,
};

static const char *months[13] = {
    "",
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
};

static const char *weekdays[7] = {
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
};

enum {ev_head, ev_tail, ev_trim, agenda_head, agenda_tail, fill, space, no_space };

struct fill {
    char *c;
    unsigned int d; /* 0 = Text, 1 = Decorative character, as in powerline */
};

const struct fill myfillers[] = {
    [ev_head].c = "\uE0B6",   /* char placed before each event */
    [ev_head].d = 1,          /* 1 if decorative character (mvwprintw uses inverted colors), 0 otherwise */
    [ev_tail].c = "\uE0B4",   /* char placed at the end of each event */
    [ev_tail].d = 1,          /* 1 if decorative character (mvwprintw uses inverted colors), 0 otherwise */
    [ev_trim].c = "...",      /* When an event summary is to long, only the chars that fit in the screen are printed, followed by `ev_trim` */
    [ev_trim].d = 0,          /* 1 if decorative character (mvwprintw uses inverted colors), 0 otherwise */
    [agenda_head].c = "   ",  /* char placed before each agenda entry */
    [agenda_head].d = 0,      /* 1 if decorative character (mvwprintw uses inverted colors), 0 otherwise */
    [agenda_tail].c = "",     /* char placed at the end of each agenda entry */
    [agenda_tail].d = 0,      /* 1 if decorative character (mvwprintw uses inverted colors), 0 otherwise */
    [fill].c = " ",           /* Generic filler */
    [fill].d = 0,             /* 1 if decorative character (mvwprintw uses inverted colors), 0 otherwise */

};

/* Actions */
enum { next_month, previous_month, go_today, up, down, left, right, disp_view_day, reload_data, quit };
const static int keys[][2] = {
    /* Keybinding,      Action */
    {'m',               next_month},
    {'M',               previous_month},
    {'g',               go_today},
    {KEY_UP,            up},
    {KEY_DOWN,          down},
    {KEY_LEFT,          left},
    {KEY_RIGHT,         right},
    {'d',               disp_view_day},
    {'r',               reload_data},
    {'q',               quit},
};
const static int len_keys = sizeof(keys)/sizeof(keys[0]);
