# skcalendar

![skcalendar](https://i.ibb.co/C8kfyQD/2022-11-13-224024-1897x1032-scrot.png)

`skcalendar` is a simple TUI calendar program for `vdirsync` calendars.
Differently from other similar programs, it allows displaying multiple calendars.
At this moment, it only allows reading vdirsync folders from which it displays:
1. A calendar for a month
2. An agenda with the upcoming events
**Note**: currently, the program does _not_ support creating, editing or removing events.

To configure it, please edit the `config.h` file directly and re-compile.
The relevant fields are:
```
struct calendar calendars[] = {
    [0].name = "Calendar 1 name",
    [0].path = "Path to calendar 1",
    [1].name = "Calendar 2 name",
    [1].path = "Path to calendar 2",
};
```
It is possible to add additional calendars, for instance, if three calendars are needed, it would read:
```
struct calendar calendars[] = {
    [0].name = "Calendar 1 name",
    [0].path = "Path to calendar 1",
    [1].name = "Calendar 2 name",
    [1].path = "Path to calendar 2",
    [2].name = "Calendar 3 name",
    [2].path = "Path to calendar 3",
};
```

`skcalendar` also displays reminders for the events by using `SIGALARM`.
The displayed message can be customized by editing the function `format_text_alarm` in `skcalendar.h`.
The current implementation calls `notify-send`, but other commands are possible.

Further customisation includes (in `config.h`):
- Naming months and weekdays.
- Selecting colours in RGB.
- Selecting the spacing between days in the calendar view.
- Defining a delimiter for each event in the calendar, including the possibility to invert colors (akin to a powerline).  
- Defining a delimiter for each agenda entry, including the possibility to invert colors (akin to a powerline).  


## Compiling
`skcalendar` depends on `ncurses` and `libical`.
It can be compiled using: `gcc skcalendar.c -lncursesw -lical -o skcalendar`
Reminders call a user-defined program (`notify-send` by default) which shall, of course, be installed.

## Usage
Keybindings can be modified by editing `config.h`
The default keybindings are:
- `UP`, `DOWN`, `LEFT` and `RIGHT` arrows provide basic movement.
- `g` moves to today.
- `d` toggles displaying the events for the selected day.
- `m` moves to next month.
- `M` moves to the previous month.
- `r` resyncs using vdirsyncer and reloads the calendar.
- `q` exits.
