#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <time.h>
#include <dirent.h>
#include <libical/ical.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <curses.h>
#include <signal.h>
#include "config.h"

/* 
 * To represent any month, a maximum of 6 weeks is required, this is 42 squares.
 * Allocate an array of windows containing 42 + 2 = 44 elements, the last two
 * are used for top and right panels.
*/
#define MAX_WINDOWS 44
#define TOP_PANEL  42
#define RIGHT_PANEL 43

/*
 * Structs
 */
struct date {
    int year;
    int month;
    int day;
    int hour;
    int minute;
};

struct events_array {
    size_t n;
    struct event *events;
};

struct event {
    int calendar;
    struct date start;
    struct date end;
    unsigned int all_day;
    char *filename;
    char *summary;
};

struct alarms_array {
    size_t n;
    struct alarm *alarms;
};

struct alarm {
    long unix_time;
    struct event *event;
};

/*
 * Functions
 */
unsigned int nc_color(unsigned int color);
char *concat(const char *s1, const char *s2);
unsigned int *count_events(const char *path);
void load_events(const char *path, struct events_array *events, struct alarms_array *alarms, const size_t i);
int get_action(const int keypress, const int keys_local[][2], int len);
unsigned int leap_year(const unsigned int year);
unsigned int days_in_month(const unsigned int year, const unsigned int month);
unsigned int weekday(unsigned int year, const unsigned int month, const unsigned int day);
void setup_ncurses(const unsigned int rgb_colors[][3], unsigned int n_colors);
char *center_text(const char *text, const unsigned int length);
int compare_times(void const *a, void const *b);
int compare_unix_times(void const *a, void const *b);
long julian_day(const struct date);
struct events_array *get_day_events(const unsigned int days_in_view, const unsigned int year, const unsigned int month, const unsigned int day, const struct events_array events);
int sel_is_current_month(const int days_in_view, const unsigned int year, const unsigned int month, const unsigned int day);
char *utf8index(char *s, size_t pos);
void utf8slice(char *s, ssize_t *start, ssize_t *end);
size_t count_utf8_code_points(const char *s);
void decorate_text(char *original_text, const char *prepend, const char *append, const char *trim, const size_t max_length, char **return_text);
int get_day(const int days_in_view, const unsigned int year, const unsigned int month, const unsigned int box);
unsigned int days_in_view(const unsigned int year, const unsigned int month);
void move_sel(const char direction, unsigned int *year, unsigned int *month, unsigned int *day, unsigned int *sel, const unsigned int d);
void print_agenda(WINDOW **window, const struct events_array events, const struct fill f[], const int color_pair, const unsigned int max_lookahead);
void beautify_str(WINDOW **window, const struct fill f[], const int color_pair, const unsigned int y, const unsigned int x, char *text, const unsigned int max_len);
int print_cal(const WINDOW *screen, WINDOW **windows, const unsigned int year, const unsigned int month, const struct appearence app, const struct events_array events, const unsigned int sel);
void clear_windows(WINDOW **windows, const int active_windows);
void increase_month(unsigned int *year, unsigned int *month);
void decrease_month(unsigned int *year, unsigned int *month);
unsigned int square_day(const unsigned int d, const unsigned int year, const unsigned int month, const unsigned int day);
void view_day(WINDOW *stdscr, WINDOW **w, unsigned int year, unsigned int month, unsigned int day, unsigned int sel, const struct events_array events, const struct appearence app);
void copy_event_struct(struct event *d, struct event *o);
void free_events_array(struct events_array *e);
void add_date_to_event(struct event *event, const icaltimetype start, const icaltimetype nd);
void cleanup(struct events_array *events, struct alarms_array *alarms, WINDOW **windows, const unsigned int n);
void update_events_to_arrays(const struct calendar calendars[], const unsigned int n_calendars, struct events_array *e, struct alarms_array *a, const unsigned int reload);

char *concat(const char *s1, const char *s2)
{
    /*
     * Concatenates two strings together
     * Returns a new pointer to char, instead of updating as strcat would do.
     *
     * Parameters:
     * - s1: first string
     * = s2: seconds string
     */

    const size_t len1 = strlen(s1);
    const size_t len2 = strlen(s2);
    char *result = malloc(len1 + len2 + 1);
    memcpy(result, s1, len1);
    memcpy(result + len1, s2, len2 + 1);
    return result;
}

unsigned int *count_events(const char *path)
{
    FILE *f = NULL;
    char *filepath = NULL;
    struct dirent *dir = NULL;
    struct stat stats;

    icalcomponent *ical = NULL;
    icalcomponent *c = NULL;
    char *ics_content = NULL;
    icalcomponent *valarm = NULL;
    icalproperty *prop = NULL;

    struct icaltimetype date;
    icaltimezone *local_tz = NULL;
    local_tz = NULL;

    /*
     * Very unsatisfying system to get the current timezone
     * Use tzset to read tzname[0] and tzname[1], the components of tznames in libical
     * Create an array containing all time zone defined in libical
     * Itarate over the array until a match is found
     * Matches need not be the correct ones, for instance, for someone in Europe/Paris
     * this procedure can match Europe/Andorra
     */

    tzset();
    char local_tzname[strlen(tzname[0]) + strlen(tzname[1]) + 1];
    sprintf(local_tzname, "%s/%s", tzname[0], tzname[1]);
    icaltimezone *ex;
    icalarray *ar = icaltimezone_get_builtin_timezones();
    unsigned int element = 0;
    unsigned int i = 0;

    unsigned int *events_alarms = calloc(2, sizeof(unsigned int));

    while(local_tz == NULL)
    {
        ex = icalarray_element_at(ar, element);
        if (strcmp(local_tzname, icaltimezone_get_tznames(ex)) == 0)
        {
            local_tz = icalarray_element_at(ar, element);
        }
        else
        {
            element++;
        }
    }
    
    DIR *directory = opendir(path);
    if (directory) {
        while ((dir = readdir(directory)) != NULL) {
            /* Skip . and .. */
            if ((strcmp(dir->d_name, ".") == 0) | (strcmp(dir->d_name, "..") == 0))
            {
                continue;
            }
            filepath = concat(path, dir->d_name);
            stat(filepath, &stats);
            f = fopen(filepath, "r");
            ics_content = calloc(stats.st_size + 1, sizeof(char));
            fread(ics_content, stats.st_size, 1, f);
            fclose(f);
            ical = icalparser_parse_string(ics_content);
            free(ics_content);
            /* Iterate over the entries in ical */
            for(c = icalcomponent_get_first_component(ical, ICAL_ANY_COMPONENT); c != 0; c = icalcomponent_get_next_component(ical, ICAL_ANY_COMPONENT))
            {
                icaltimetype start = icalcomponent_get_dtstart(c);
                icaltimetype end = icalcomponent_get_dtend(c);
                icaltimezone_convert_time(&start, start.zone, local_tz);
                icaltimezone_convert_time(&end, end.zone, local_tz);
                if ((start.year >= min_year) & (start.year <= max_year))
                {
                    events_alarms[0] = events_alarms[0] + 1;
                    for(valarm = icalcomponent_get_first_component(c, ICAL_VALARM_COMPONENT); valarm; valarm = icalcomponent_get_next_component(c, ICAL_VALARM_COMPONENT))
                    {
                        if(prop = icalcomponent_get_first_property(valarm, ICAL_TRIGGER_PROPERTY))
                        {
                            events_alarms[1] += 1;
                        }
                    }
                }
                icalcomponent_free(c);
		icalcomponent_free(valarm);
            }
            free(filepath);
            icalcomponent_free(ical);
        }
    closedir(directory);
    }
    return events_alarms;
}

void load_events(const char *path, struct events_array *events, struct alarms_array *alarms, const size_t i)
{
    /*
     * Loads the calendar events listed in path, filling a events_array struct.
     *
     * Parameters:
     * - path: path containing the ical files to be parsed and loaded
     * - events: events_array struct where the events will be loaded to
     * - i: calendar identifier.
     *      This information is incorporated in the events array within events_array
     *      and is later used to print colored entries depending on the calendar identifier
     */

    FILE *f = NULL;
    char *filepath = NULL;
    struct dirent *dir = NULL;
    struct stat stats;

    icalcomponent *ical = NULL;
    icalcomponent *c = NULL;
    char *ics_content = NULL;

    struct icaltimetype date;
    icaltimezone *local_tz = NULL;
    local_tz = NULL;

    /*
     * Very unsatisfying system to get the current timezone
     * Use tzset to read tzname[0] and tzname[1], the components of tznames in libical
     * Create an array containing all time zone defined in libical
     * Itarate over the array until a match is found
     * Matches need not be the correct ones, for instance, for someone in Europe/Paris
     * this procedure can match Europe/Andorra
     */

    tzset();
    char local_tzname[strlen(tzname[0]) + strlen(tzname[1]) + 1];
    sprintf(local_tzname, "%s/%s", tzname[0], tzname[1]);
    icaltimezone *ex;
    icalarray *ar = icaltimezone_get_builtin_timezones();
    unsigned int element = 0;

    /*
     * Alarms
     */
    icalcomponent *valarm = NULL;
    icalproperty *prop = NULL;
    struct icaltriggertype trigger;
    icaltimetype tmp;
    /*
     * It is easier to work out the alarms in UTC
     * We will use the alarm signal, which requires a number of seconds
     * Thus, we will transform the date-time of an alarm (in UTC) to unix timestamp
     * and compare it to the current value of the unix timestamp.
     */
    icaltimezone *utc = icaltimezone_get_utc_timezone();

    while(local_tz == NULL)
    {
        ex = icalarray_element_at(ar, element);
        if (strcmp(local_tzname, icaltimezone_get_tznames(ex)) == 0)
        {
            local_tz = icalarray_element_at(ar, element);
        }
        else
        {
            element++;
        }
    }
    
    DIR *directory = opendir(path);
    if (directory) {
        while ((dir = readdir(directory)) != NULL) {
            /* Skip . and .. */
            if ((strcmp(dir->d_name, ".") == 0) | (strcmp(dir->d_name, "..") == 0))
            {
                continue;
            }
            filepath = concat(path, dir->d_name);
            stat(filepath, &stats);
            f = fopen(filepath, "r");
            ics_content = calloc(stats.st_size + 1, sizeof(char));
            fread(ics_content, stats.st_size, 1, f);
            fclose(f);
            ical = icalparser_parse_string(ics_content);
            free(ics_content);
            /* Iterate over the entries in ical */
            for(c = icalcomponent_get_first_component(ical, ICAL_ANY_COMPONENT); c != 0; c = icalcomponent_get_next_component(ical, ICAL_ANY_COMPONENT))
            {
                icaltimetype start = icalcomponent_get_dtstart(c);
                icaltimetype end = icalcomponent_get_dtend(c);
                icaltimezone_convert_time(&start, start.zone, local_tz);
                icaltimezone_convert_time(&end, end.zone, local_tz);
                if ((start.year >= min_year) & (start.year <= max_year))
                {
                    events->events[events->n].calendar = i;
                    add_date_to_event(&events->events[events->n], start, end);

                    events->events[events->n].filename = malloc(sizeof(char)*(1 + strlen(filepath)));
                    strcpy(events->events[events->n].filename, filepath);
                    events->events[events->n].summary = malloc(sizeof(char)*(1 + strlen(icalcomponent_get_summary(c))));
                    sprintf(events->events[events->n].summary, "%s", icalcomponent_get_summary(c));
                    /*
                    * Alarms
                    * Adapted from: https://github.com/asterisk/asterisk/blob/master/res/res_calendar_icalendar.c
                    */
                    for(valarm = icalcomponent_get_first_component(c, ICAL_VALARM_COMPONENT); valarm; valarm = icalcomponent_get_next_component(c, ICAL_VALARM_COMPONENT))
                    {

                        if(prop = icalcomponent_get_first_property(valarm, ICAL_TRIGGER_PROPERTY))
                        {
                            //alarms->alarms = realloc(alarms->alarms, sizeof(struct alarm)*(alarms->n + 1));
                            //alarms->alarms[alarms->n].event = malloc(sizeof(struct event));
                            //copy_event_struct(alarms->alarms[alarms->n].event, &events->events[events->n]);
                            alarms->alarms[alarms->n].event = &events->events[events->n];

                            trigger = icalproperty_get_trigger(prop);

                            if (!icaltime_is_null_time(trigger.time)) 
                            { 
                                /* This is an absolute time */
                                tmp = icaltime_convert_to_zone(trigger.time, utc);
                                alarms->alarms[alarms->n].unix_time = icaltime_as_timet(tmp);
                            } 
                            else
                            { 
                                icaltimezone_convert_time(&start, start.zone, utc);
                                tmp = icaltime_add(start, trigger.duration);
                                alarms->alarms[alarms->n].unix_time = icaltime_as_timet(tmp);
                            }
                            alarms->n += 1;
                        }
                    }
                    events->n++;
                }
                icalcomponent_free(c);
		icalcomponent_free(valarm);
            }
            free(filepath);
            icalcomponent_free(ical);
        }
    closedir(directory);
    }
}

int get_action(const int keypress, const int keys_local[][2], int len)
{
    unsigned int i = 0;

    if (keypress == -1)
    {
        return -1;
    }
    for (i = 0; i < len; i++)
    {
        if (keypress == keys_local[i][0])
        {
            return keys_local[i][1];
        }
    }
    return -1;
}

unsigned int leap_year(const unsigned int year)
{
    /* 
     * If year is a leap year, returns 1
     * Otherwise returns 0
     *
     * Parameters:
     * - year: the year to test for leap year
     */

    /*
     * https://stackoverflow.com/questions/3220163/how-to-find-leap-year-programmatically-in-c
     */
    if ((year & 3) == 0 && ((year % 25) != 0 || (year & 15) == 0)) { 
        return 1;
    }
    else {
        return 0;
    }
}

unsigned int days_in_month(const unsigned int year, const unsigned int month)
{
    /*
     * Returns the number of days in a given month
     *
     * Parameters:
     * - year: year
     * - month: month for which to compute the number of days
     */

    /*                          Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec */ 
    static const char days[] = {31,  28,  31,  30,  31,  30,  31,  31,  30,  31,  30,  31};
    return leap_year(year) && month == 2 ? 29 : days[month-1];
}

unsigned int weekday(unsigned int year, const unsigned int month, const unsigned int day) 
{
    /* 
     * Given a date (year, month, day), returns its corresponding weekday,
     * this is, an integer representing Monday, Tuesday, etc.
     * In this implementation, Moday is 0
     *
     * Parameters:
     * - year: year
     * - month: month
     * - day: day for which to compute its weekday
     */

    /* 
     * https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week#Implementation-dependent_methods
     * Original function gives 0 for Suday, 1 for Monday, etc 
     */
    static unsigned char t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    unsigned int dow;

    if ( month < 3 )
    {
        year -= 1;
    }

     dow = (year + year/4 - year/100 + year/400 + t[month - 1] + day) % 7;

     /* Change the function to return 0 on Monday, 1 on Tuesday, etc. */
     return dow == 0 ? 6 : dow - 1;
}

unsigned int nc_color(unsigned int color)
{
    /* 
     * Color redefinitions
     * In ncurses, colors are scaled from 0 to 1000
     * Use a function to do the scaling
    */
    float a = color*1000;
    float b = 255;
    float div = a/b;
    unsigned int z = div/1;
    return z;
}

void setup_ncurses(const unsigned int rgb_colors[][3], unsigned int n_colors)
{
    enum { fg, bg };
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int nc[n_colors][3];

    setlocale(LC_ALL, "");
    initscr();
    clear();
    noecho();
    curs_set(0);
    cbreak();
    halfdelay(1);
    keypad(stdscr, TRUE);
    start_color();

    /*
     * Fill the matrix nc with the ncurses colorcodes
     * that are equivalent to the RGB values
     */
    for (i = 0; i < n_colors; i++)
    {
        for(j = 0; j < 3; j++)
        {
            nc[i][j] = nc_color(rgb_colors[i][j]);
        }
    }

    /*
     * Initizalize the ncurses colors
     */
    for (i = cal_1; i < cal_1 + n_colors; i++)
    {
        init_color(i, nc[i - cal_1][0], nc[i - cal_1][1], nc[i - cal_1][2]);
    }

    unsigned int a = 0;
    unsigned int pair = cal_1;

    /*
     * Setup color pairs
     */
    while(n_colors > 0)
    {
        init_pair(pair,       cal_1 + a,     cal_1 + a + 1);
        /* 
         * Store the inverted colors in the range 200
         * We use these for filler.d
         */
        init_pair(pair + 100, cal_1 + a + 1, 0);
        a += 2;
        pair++;
        n_colors--;
    }
    init_pair(99, 0, 0);

    return;
}

char *center_text(const char *text, const unsigned int length)
{

    char *centered_text = calloc(length + 1, sizeof(char));
    unsigned int i = (length - strlen(text))/2;
    unsigned int c = 0;

    for (c = 0; c < i; c++)
    {
        centered_text[c] = ' ';
    }
    for (c = i; c < i + strlen(text); c++)
    {
        centered_text[c] = text[c - i];
    }
    for (c = i + strlen(text); c < length; c++)
    {
        centered_text[c] = ' ';
    }
    centered_text[length] = '\0';
    return centered_text;
}

int compare_times(void const *a, void const *b)
{
    const struct event *ev1 = a;
    const struct event *ev2 = b;

    const unsigned int t1 = ev1->start.hour * 60 + ev1->start.minute;
    const unsigned int t2 = ev2->start.hour * 60 + ev2->start.minute;
    if (t1 == t2)
    {
        return 0;
    }
    else if (t1 > t2)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

int compare_unix_times(void const *a, void const *b)
{
    const struct alarm *ev1 = a;
    const struct alarm *ev2 = b;

    const unsigned int t1 = ev1->unix_time;
    const unsigned int t2 = ev2->unix_time;

    if (t1 == t2)
    {
        return 0;
    }
    else if (t1 > t2)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

long julian_day(const struct date date)
{
    /* https://pdc.ro.nu/jd-code.html */
    unsigned int y = date.year;
    unsigned int m = date.month;
    const unsigned int d = date.day;

    y += 8000;
    if (m < 3 ) 
    {
        y--; 
        m+=12; 
    }
    return (y*365) +(y/4) -(y/100) +(y/400) -1200820 +(m*153+3)/5-92 +d-1;
}


struct events_array *get_day_events(const unsigned int days_in_view, const unsigned int year, const unsigned int month, const unsigned int day, const struct events_array events)
{
    unsigned int i = 0;
    struct events_array *events_array = NULL;
    events_array = malloc(sizeof(struct events_array));
    events_array->events = NULL;
    events_array->n = 0;

    const struct date today = {
        .year = year,
        .month = month + sel_is_current_month(days_in_view, year, month, day),
        .day = get_day(days_in_view, year, month, day),
    };

    for (i = 0; i < events.n; i++)
    {
        /* Obtain events starting at the requested day */
        if ((julian_day(events.events[i].start) <= julian_day(today)) && (julian_day(today) <= julian_day(events.events[i].end)))
        {
            /* End date for all-day events is non-inclusive, meaning that these will appear as finishing the day after they actually end
             * https://stackoverflow.com/questions/29741546/rfc2445-how-to-store-all-day-events */
            if ((events.events[i].all_day == 1) && julian_day(today) == julian_day(events.events[i].end))
            {
                /* Do not add the event for the last day */
                continue;
            }

            events_array->events = realloc(events_array->events, sizeof(struct event)*(events_array->n+1));
            /* Initialize the element */
            events_array->events[events_array->n].filename = NULL;
            events_array->events[events_array->n].summary = NULL;
            events_array->events[events_array->n].start.year = 0;
            events_array->events[events_array->n].start.month = 0;
            events_array->events[events_array->n].start.day = 0;
            events_array->events[events_array->n].start.hour = 0;
            events_array->events[events_array->n].start.minute = 0;
            events_array->events[events_array->n].end.year = 0;
            events_array->events[events_array->n].end.month = 0;
            events_array->events[events_array->n].end.day = 0;
            events_array->events[events_array->n].end.hour = 0;
            events_array->events[events_array->n].end.minute = 0;
            events_array->events[events_array->n].calendar = 0;
            events_array->events[events_array->n].all_day = 0;

            copy_event_struct(&events_array->events[events_array->n], &events.events[i]);

            /* If the even started before today, set initial time for today at 00:00
             * This only applies to events that are NOT all-day */
            if ((julian_day(events_array->events[events_array->n].start) < julian_day(today)) && (events_array->events[events_array->n].all_day == 0))
            {
                events_array->events[events_array->n].start.hour = 0;
                events_array->events[events_array->n].start.minute = 0;
            }
            /* If the event finishes after today, set end time for today at 23:59
             * This only applies to events that are NOT all-day */
            if ((julian_day(events_array->events[events_array->n].end) > julian_day(today)) && (events_array->events[events_array->n].all_day == 0))
            {
                events_array->events[events_array->n].end.hour = 23;
                events_array->events[events_array->n].end.minute = 59;
            }


            events_array->n++;
        }
    }
    qsort(events_array->events, events_array->n, sizeof(struct event), compare_times);
    return events_array;
}
            
int sel_is_current_month(const int days_in_view, const unsigned int year, const unsigned int month, const unsigned int day)
{
    /*
     * If the selected day belongs to the current month, returns 0
     * Otherwise, if it belongs to the previous month returns -1
     * and if it belongs to the next month returns 1
     * 
     * Parameters:
     * - days_in_view: number of days displayed in the calendar
     *                 if days_in_view = -1, returns 0
     * - year: current year
     * - month: current month
     * - day: day for which to test its belonging to month
     */

    const unsigned char n = days_in_month(year, month);
    const unsigned char month_starts = weekday(year, month, 1);

    if (days_in_view == -1)
    {
        return 0;
    }

    if (day < month_starts)
    {
        return -1;
    }
    else if ((day >= month_starts) & (day - month_starts + 1 <= n))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

char *utf8index(char *s, size_t pos)
{    
    /* 
     * Returns the number of utf8 code points in the buffer at s
     *
     * https://stackoverflow.com/questions/7298059/how-to-count-characters-in-a-unicode-string-in-c 
    */

    ++pos;
    for (; *s; ++s) {
        if ((*s & 0xC0) != 0x80)
        {
            --pos;
        }
        if (pos == 0) 
        {
            return s;
        }
    }
    return NULL;
}

void utf8slice(char *s, ssize_t *start, ssize_t *end)
{
    /* 
     * Converts codepoint indexes start and end to byte offsets in the buffer at s
     *
     * https://stackoverflow.com/questions/7298059/how-to-count-characters-in-a-unicode-string-in-c
    */

    char *p = utf8index(s, *start);
    *start = p ? p - s : -1;
    p = utf8index(s, *end);
    *end = p ? p - s : -1;
}

size_t count_utf8_code_points(const char *s) {
    /* 
     * https://stackoverflow.com/questions/32936646/getting-the-string-length-on-utf-8-in-c
    */

    size_t count = 0;
    while (*s) {
        count += (*s++ & 0xC0) != 0x80;
    }
    return count;
}

void decorate_text(char *original_text, const char *prepend, const char *append, const char *trim, const size_t max_length, char **return_text)
{

    const unsigned int original_text_len = count_utf8_code_points(original_text);
    const unsigned int prepend_len = count_utf8_code_points(prepend);
    const unsigned int append_len = count_utf8_code_points(append);
    const unsigned int trim_len = count_utf8_code_points(trim);

    const unsigned int char_len = original_text_len + prepend_len + append_len;
    const unsigned int byte_len = snprintf(NULL, 0, "%s%s%s", prepend, original_text, append);

    *return_text = calloc(byte_len + max_length - original_text_len + 1, sizeof(char));

    if (char_len <= max_length)
    {
        sprintf(*return_text, "%s%s%*s", prepend, 
                                         original_text, 
                                         max_length - prepend_len - original_text_len, 
                                         append);
    }
    else
    {
        size_t start = 0;
        size_t end = max_length - prepend_len - trim_len - append_len;
        utf8slice(original_text, &start, &end);
        sprintf(*return_text, "%s%.*s%s%s", prepend,
                                            end,
                                            original_text,
                                            trim,
                                            append);
    }
}

int get_day(const int days_in_view, const unsigned int year, const unsigned int month, const unsigned int box)
{
    /* 
     * Returns the number of day (1st, 2nd, 3rd, etc) corresponding to 
     * the selected box.
     *
     * Parameters:
     * - days_in_view: number of days displayed in the calendar
     *                 if days_in_view = -1 returns the selected day
     * - year: current year
     * - month: current month
     * - box: box (within days_in_view) for which to compute its corresponding day
     */

    const unsigned char n_days = days_in_month(year, month);
    const unsigned char month_starts = weekday(year, month, 1);
    const unsigned char previous_month = month - 1 == 0 ? 12 : month - 1;
    const unsigned char n_days_previous = days_in_month(year, previous_month);

    if (days_in_view == -1)
    {
        return box;
    }

    if (box < month_starts)
    {
        return n_days_previous - month_starts + box + 1;
    }
    else if ((box >= month_starts) & (box - month_starts + 1 <= n_days))
    {
        return box - month_starts + 1;
    }
    else
    {
        return box - n_days - month_starts + 1;
    }
}

unsigned int days_in_view(const unsigned int year, const unsigned int month)
{
    /* 
     * Returns the number of squares to print for a given month
     * Compute it as follows:
     * If the month begins in Monday and finshed in Sunday:
     *    number of days in the month
     * Else:
     *    add the number of weekdays until the first
     *    6 - number of weekdays from the thirty-first
     *
     * Parameters:
     * - year: year
     * - month: month for which to compute the number of days in view
     */

    const unsigned int d = days_in_month(year, month);
    return d + weekday(year, month, 1) + 6 - weekday(year, month, d);
}

void move_sel(const char direction, unsigned int *year, unsigned int *month, unsigned int *day, unsigned int *sel, const unsigned int d)
{

    /* 
     * Moves the selected day (*sel) considering that:
     * Moving one row up may decrease the month/year
     * Moving one row down may increase the month/year
     * Moving to the left may decrease the month/year
     * Moving to the right may increase the month/year
     *
     * Parameters:
     *  - direction: movement direction, as defined in config.h
     *  - day: value of the currently selected day, it gets updated
     *  - month: value of the currently selected month, it gets updated
     *  - year: value of the currently selected year, it gets updated
     *  - sel: currently selected day, it gets updated
     *  - d: number of days in display
     */
    switch(direction)
    {
        case right:
            if (*sel < d - 1)
            {
                *sel = *sel + 1;
            }
            else
            {
                *sel = 0;
                increase_month(year, month);
            }
            break;
        case left:
            if (*sel > 0)
            {
                *sel = *sel - 1;
            }
            else
            {
                decrease_month(year, month);
                const unsigned int a = *year;
                const unsigned int b = *month;
                *sel = days_in_view(a, b) - 1;
            }
            break;
        case down:
            if (*sel < d - 7)
            {
                *sel = *sel + 7;
            }
            else
            {
                *sel = *sel%7;
                increase_month(year, month);
            }
            break;
        case up:
            if (*sel > 6)
            {
                *sel = *sel - 7;
            }
            else
            {
                decrease_month(year, month);
                const unsigned int a = *year;
                const unsigned int b = *month;
                *sel = *sel%7 + days_in_view(a, b) - 7;
            }
            break;
    }
    *day = get_day(days_in_view(*year, *month), *year, *month, *sel);
}

void print_agenda(WINDOW **window, const struct events_array events, const struct fill f[], const int color_pair, const unsigned int max_lookahead)
{
    /*
     * Prints the first `max_lookahed` upcoming events
     * If the number of events to be printed is greater than the available space, the function prints all that fit
     *
     * Parameters:
     * - window: the window to modify
     * - events: a struct containing all the loaded events
     * - f[]: fillers to decorate the events, defined in config.h
     * - color_pair: color pair to colorize the events, not used
     * - max_lookahead: maximum number of elements to load, defined in config.h
     */

    unsigned int printed_events = 0;
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int k = 0;
    unsigned int x = 0;
    unsigned int y = 0;
    const time_t today = time(NULL);

    getmaxyx(*window, y, x);
    const struct tm tm = *localtime(&today);
    unsigned int year = tm.tm_year + 1900;
    unsigned int month = tm.tm_mon + 1;
    unsigned int day = tm.tm_mday;

    char *txt_agenda = NULL;
    char *txt_raw = NULL;
    unsigned int len_txt_raw = 0;

    struct events_array *events_array = NULL; 
   
    while ((i < max_lookahead) & (y > 0))
    {
        events_array = get_day_events(-1, year, month, day, events);
        for (j = 0; j < events_array->n; j++)
        {
            if (j == 0)
            {
                if (printed_events > 0)
                {
                    printed_events++;
                    y--;
                }

                mvwprintw(*window, printed_events, 0, "%02d-%02d-%d", day, month, year);
                printed_events++;
                y--;
            }

            len_txt_raw = snprintf(NULL, 0, "%02d:%02d %s", events_array->events[j].start.hour, events_array->events[j].start.minute, events_array->events[j].summary);
            txt_raw = calloc((len_txt_raw + 1), sizeof(char));
            sprintf(txt_raw, "%02d:%02d %s", events_array->events[j].start.hour, events_array->events[j].start.minute, events_array->events[j].summary);
            decorate_text(txt_raw, f[agenda_head].c, f[agenda_tail].c, f[fill].c, x, &txt_agenda);
            free(txt_raw);
            txt_raw = NULL;
            mvwprintw(*window, printed_events, 0, "%s", txt_agenda);
            free(txt_agenda);
            txt_agenda = NULL;
            printed_events++;
            y--;
        }
        i++;

        if (day + 1 <= days_in_month(year, month))
        {
            day++;
        }
        else if (month < 13)
        {
            month++;
            day = 1;
        }
        else
        {
            year++;
            month = 1;
            day = 1;
        }
        free_events_array(events_array);
        free(events_array->events);
        events_array->events = NULL;
    }
    wrefresh(*window);
}

void beautify_str(WINDOW **window, const struct fill f[], const int color_pair, const unsigned int y, const unsigned int x, char *text, const unsigned int max_len)
{

    /*
     * Decorates a string by adding fillers and prints it to ncurses
     * using the suplied colorscheme
     * 
     * Parameters:
     * - window: the window to modify
     * - f[]: fillers to decorate the events, defined in config.h
     * - color_pair: color pair to colorize the event
     * - y: y-coordinate on which to print using ncurses
     * - x: x-coordinate on which to print using ncurses
     * - text: the text to be decorated and printed
     * - max_len: maximum available space for printing (along the x axis)
     *            if the string to be printed (after decoration) is longer
     *            it gets trimmed using the trimers defined by f[]
     */ 

    unsigned int len = 0;
    char *print_text = NULL;

    const unsigned int char_len_prepend = count_utf8_code_points(f[ev_head].c);
    const unsigned int char_len_append  = count_utf8_code_points(f[ev_tail].c);
    
    /* 
     * The COLOR_PAIR below are offseted by 100 and 200, to avoid changing the 
     * user's colorscheme in the terminal.
     * It is unlikely to use so many colors.
    */
    if ((f[ev_head].d == 1) & (f[ev_tail].d == 1))
    {
        len = max_len - char_len_prepend - char_len_append;
        wattron(*window, COLOR_PAIR(200 + color_pair));
        mvwaddstr(*window, y, 0, f[ev_head].c);
        decorate_text(text, "", "", f[ev_trim].c, len, &print_text);
        wattron(*window, COLOR_PAIR(100 + color_pair));
        mvwaddstr(*window, y, char_len_prepend, print_text);
        wattron(*window, COLOR_PAIR(200 + color_pair));
        mvwaddstr(*window, y, max_len - char_len_append, f[ev_tail].c);
        wattroff(*window, COLOR_PAIR(200 + color_pair));
    }
    if ((f[ev_head].d == 1) & (f[ev_tail].d == 0))
    {
        len = max_len - char_len_prepend;
        wattron(*window, COLOR_PAIR(200 + color_pair));
        mvwaddstr(*window, y, 0, f[ev_head].c);
        wattron(*window, COLOR_PAIR(100 + color_pair));
        decorate_text(text, "", f[ev_tail].c, f[ev_trim].c, len, &print_text);
        mvwaddstr(*window, y, char_len_prepend, print_text);
        wattroff(*window, COLOR_PAIR(100 + color_pair));
    }
    if ((f[ev_head].d == 0) & (f[ev_tail].d == 1))
    {
        len = max_len - char_len_append;
        wattron(*window, COLOR_PAIR(100 + color_pair));
        decorate_text(text, f[ev_head].c, "", f[ev_trim].c, len, &print_text);
        mvwaddstr(*window, y, 0, print_text);
        wattron(*window, COLOR_PAIR(200 + color_pair));
        mvwaddstr(*window, y, max_len - char_len_append, f[ev_tail].c);
        wattroff(*window, COLOR_PAIR(200 + color_pair));
    }
    if ((f[ev_head].d == 0) & (f[ev_tail].d == 0))
    {
        len = max_len;
        decorate_text(text, f[ev_head].c, f[ev_tail].c, f[ev_trim].c, max_len, &print_text);
        wattron(*window, COLOR_PAIR(100 + color_pair));
        mvwaddstr(*window, y, 0, print_text);
        wattroff(*window, COLOR_PAIR(100 + color_pair));
    }
    free(print_text);
    print_text = NULL;
}

int print_cal(const WINDOW *screen, WINDOW **windows, const unsigned int year, const unsigned int month, const struct appearence app, const struct events_array events, const unsigned int sel)
{

    /*
     * Prints the calendar for the currently selected month
     *
     * Parameters:
     * - screen: main screen in ncurses
     * - windows: array containing the days that conform the view of the month
     * - month: current month
     * - year: current year
     * - app: struct detailing the apparence of the different elements, borders, etc
     *        defined in config.h
     * - day: a struct containing an array with all the loaded events
     * - sel: currently selected day
     */

    unsigned int i = 0;
    unsigned int j = 1;
    unsigned int event = 0;
    unsigned int x = 0;
    unsigned int y = 0 ;
    unsigned int c = 0;

    struct events_array *events_array;
    getmaxyx(screen, y, x);

    const unsigned int d = days_in_view(year, month);
    const unsigned int rows = d / 7;
    const unsigned int day_y = (y - app.size_top) / rows - app.y_border;
    const unsigned int day_x = (x * (app.size_agenda - 1)) / (app.size_agenda * 7);
    const unsigned int agenda_y = y - app.size_top;
    const unsigned int agenda_x = x - (day_x + app.x_border)*7;

    char shortened_summaries[day_x + 1];
    char *text_to_center = NULL;
    char *centered_text = NULL;

    const unsigned int day = get_day(d, year, month, sel);

    /* If the current selection is smaller than weekday of the first day of the current month
     * then the selection corresponds to the previous month.
     * Similarly, if the current selection is greater than the number of days in the month plus
     * the weekday of the first day (this is, offseted by the number of squares that belong to the 
     * previous mounth), then the selection corresponds to the next month
     * Proceed similarly with the year
     */

    int offset_month = 0;
    int offset_year = 0;

    if (sel < weekday(year, month, 1))
    {
        if (month == 1)
        {
            offset_month = 11;
            offset_year = -1;
        }
        else
        {
            offset_month = -1;
        }
    }
    if (sel > days_in_month(year, month) + weekday(year, month, 1) - 1)
    {
        if (month == 12)
        {
            offset_month = -11;
            offset_year = 1;
        }
        else
        {
            offset_month = 1;
        }
    }

    /* Print the cells for the days in the view */
    for (i = 0; i < rows; i++)
    {
        for (j = 0; j < 7; j++)
        {
            windows[7*i + j] = newwin(day_y, day_x,  app.size_top + i*(day_y + app.y_border), j*(day_x + app.x_border));
            whline(windows[7*i + j], ACS_HLINE, day_x);
            if (sel == 7*i + j)
            {
                box(windows[7*i + j], ACS_VLINE, ACS_HLINE);
            }
            mvwprintw(windows[7*i + j], 0, 0, "%d", get_day(d, year, month, 7*i + j));
            events_array = get_day_events(d, year, month, 7*i + j, events);
            if (events_array->n <= day_y)
            {
                for (event = 0; event < events_array->n; event++)
                {
                    beautify_str(&windows[7*i + j], myfillers, events_array->events[event].calendar, event + 1, 0, events_array->events[event].summary, day_x);
                }
            }
            else
            {
                for (event = 0; event < day_y - 1; event++)
                {
                    beautify_str(&windows[7*i + j], myfillers, events_array->events[event].calendar, event + 1, 0, events_array->events[event].summary, day_x);
                }
                beautify_str(&windows[7*i + j], myfillers, events_array->events[event].calendar, event + 1, 0, "", day_x);
            }
            wrefresh(windows[7*i + j]);
            if (events_array->n > 0)
            {
                free_events_array(events_array);
                free(events_array);
                events_array = NULL;
            }
        }
    }

    /* Top window, contains month name and year, and weekdays */
    text_to_center = malloc(sizeof(char)*(1 + snprintf(NULL, 0, "%d-%s-%d", day, months[month + offset_month], year + offset_year)));
    sprintf(text_to_center, "%d-%s-%d", day, months[month + offset_month], year + offset_year);
    centered_text = malloc(sizeof(char)*(1 + snprintf(NULL, 0, "%s", text_to_center)));
    centered_text = center_text(text_to_center, (day_x+app.y_border)*7);
    free(text_to_center);
    text_to_center = NULL;
    windows[TOP_PANEL] = newwin(app.size_top, x, 0, 0);
    mvwprintw(windows[TOP_PANEL], 0, 0, "%s", centered_text);
    free(centered_text);
    for (j = 0; j < 7; j++)
    {
        mvwprintw(windows[TOP_PANEL], app.size_top - 1, j*(day_x + app.x_border), weekdays[j]);
        wrefresh(windows[TOP_PANEL]);
    }

    /* Mini agenda */
    windows[RIGHT_PANEL] = newwin(y - app.size_top, agenda_x, app.size_top, (day_x + app.x_border)*7); 
    print_agenda(&windows[RIGHT_PANEL], events, myfillers, 1, 7);

    return d;
}

void clear_windows(WINDOW **windows, const int active_windows)
{
    /*
     * Clears and erases the windows in an array of windows
     *
     * Parameters:
     * - windows: array of windows, including those to be cleaned and erased
     * - active_windows: the array of windows is declared including the maximum
     *                   number of windows that may exist for any given month.
     *                   Thus, in a given month, some of these windows may be
     *                   NULL pointers.
     *                   In particular, months can have either 4, 5 or 6 weeks
     *                   For months with 4 or 5 weeks, the entire MAX_WINDOWS 
     *                   are not allocated.
     *                   active_windows indicates the number of windows not
     *                   pointing to NULL.
     */
    
    unsigned int i = 0;

    for (i = 0; i < active_windows; i++)
    {
        wbkgd(windows[i], COLOR_PAIR(99));
        werase(windows[i]);
        wrefresh(windows[i]);
        delwin(windows[i]);
    }
    /* Delete the top, bottom and right panes
     * If only one window is passed, do nothing */
    if (i > 1)
    {
        for (i = TOP_PANEL; i < MAX_WINDOWS; i++)
        {
            wbkgd(windows[i], COLOR_PAIR(99));
            wclear(windows[i]);
            wrefresh(windows[i]);
            delwin(windows[i]);
        }
    }
}

void increase_month(unsigned int *year, unsigned int *month)
{
    /*
     * Increases the value of month.
     * If the month is December, it sets it as January and updates the year.
     *
     * Parameters:
     * - month: month to be increased by 1.
     *          If month is 12, sets month equal to 1 and increases year by one.
     * - year: current year.
     *         Year is increased by 1 whenever month = 12
     */

    if (*month == 12)
    {
        *month = 1;
        *year = *year + 1;
    }
    else
    {
        *month = *month + 1;
    }
}

void decrease_month(unsigned int *year, unsigned int *month)
{
    /*
     * Decreases the value of month.
     * If the month is January, it sets it as December and updates the year.
     *
     * Parameters:
     * - year: current year.
     *         Year is decreased by 1 whenever month = 1
     * - month: month to be increased by 1.
     *          If month is 1, sets month equal to 12 and decreases year by one.
     */

    if (*month == 1)
    {
        *month = 12;
        *year = *year - 1;
    }
    else
    {
        *month = *month - 1;
    }
}

unsigned int square_day(const unsigned int d, const unsigned int year, const unsigned int month, const unsigned int day)
{

    /* 
     * Given day, month and year; returns the square in the calendar corresponding
     * to that day
     *
     * Parameters:
     * - d: number of days in current view
     * - month: current month
     * - year: current yer
     * - day: current day
     */

    unsigned int i = 0;
    while(get_day(d, year, month, i) > 1)
    {
        i++;
    }
    while(get_day(d, year, month, i) != day)
    {
        i++;
    }
    return i;
}

void view_day(WINDOW *stdscr, WINDOW **w, unsigned int year, unsigned int month, unsigned int day, unsigned int sel, const struct events_array events, const struct appearence app)
{

    unsigned int stdscr_y;
    unsigned int stdscr_x;
    getmaxyx(stdscr, stdscr_y, stdscr_x);

    unsigned int y = stdscr_y - 2*app.view_day_margins;
    const unsigned int x = stdscr_x - 2*app.view_day_margins;
    unsigned int i = 0;

    w[0] = newwin(y, x, app.view_day_margins, app.view_day_margins);
    box(w[0], 0, 0);

    char *raw_txt = NULL;
    unsigned int len_raw_txt = 0 ;
    char *txt = NULL;

    struct events_array *e = NULL;

    month = month + sel_is_current_month(days_in_view(year, month), year, month, sel);
    e = get_day_events(-1, year, month, day, events);
    
    mvwprintw(w[0], 1, 1, "%02d-%02d-%d", day, month, year);
    while((e->n > 0) & (y>1))
    {
        mvwprintw(w[0], i+2, 1, "   %02d:%02d-%02d:%02d %s", 
                e->events[i].start.hour, e->events[i].start.minute,  
                e->events[i].end.hour, e->events[i].end.minute,  
                e->events[i].summary);
        i++;
        e->n--;
        y--;
    }
    wrefresh(w[0]);
    free_events_array(e);
    free(e->events);
    e->events = NULL;
}

void free_events_array(struct events_array *e)
{
    unsigned int i = 0;

    for (i = 0; i < e->n; i++)
    {
        free(e->events[i].filename);
        e->events[i].filename = NULL;
        free(e->events[i].summary);
        e->events[i].summary = NULL;
    }
}

void copy_event_struct(struct event *d, struct event *o)
{
    d->summary = calloc(strlen(o->summary) + 1, sizeof(char));
    d->filename = calloc(strlen(o->filename) + 1, sizeof(char));

    strcpy(d->summary, o->summary);
    strcpy(d->filename, o->filename);

    d->start = o->start;
    d->end = o->end;
    d->calendar = o->calendar;
}

unsigned int setup_alarm(struct alarms_array alarms)
{
    const time_t t = time(NULL);
    unsigned int i = 0;

    while ((alarms.alarms[i].unix_time < t) && (i < alarms.n))
    {
        i++;
    }
    alarm(alarms.alarms[i].unix_time - t);
    return i;
}

volatile sig_atomic_t set_alarm = 0;
volatile sig_atomic_t notify_alarm = 0;
void sig_handler(int signum)
{
    set_alarm = 1;
    notify_alarm = 1;
}

void add_date_to_event(struct event *event, const icaltimetype start, const icaltimetype end)
{
    /* All-day event */
    if (start.is_date == 1)
    {
        event->all_day      = 1;
        /* Event start */
        event->start.year   = start.year;
        event->start.month  = start.month;
        event->start.day    = start.day;
        /* Event end */
        event->end.year     = end.year;
        event->end.month    = end.month;
        event->end.day      = end.day;
    }
    else
    {
        event->all_day      = 0;
        /* Event start */
        event->start.year   = start.year;
        event->start.month  = start.month;
        event->start.day    = start.day;
        event->start.hour   = start.hour;
        event->start.minute = start.minute;
        /* Event end */
        event->end.year     = end.year;
        event->end.month    = end.month;
        event->end.day      = end.day;
        event->end.hour     = end.hour;
        event->end.minute   = end.minute;
    }
}

void format_text_alarm(char **text_alarm, const struct alarm alarm)
{
    const time_t t = time(NULL);
    const struct tm tm = *localtime(&t);
    
    const struct date d = {
        .day = tm.tm_mday,
        .month = tm.tm_mon + 1,
        .year = tm.tm_year + 1900,
    };

    unsigned int len = 0;
    const int diff_in_days = julian_day(alarm.event->start) - julian_day(d);
    switch(diff_in_days)
    {
        /* Event happens today */
        case 0:
            len = snprintf(NULL, 0, "notify-send \"Reminder\" \"%s today at %02d:%02d.\"", alarm.event->summary, alarm.event->start.hour, alarm.event->start.minute);
            *text_alarm = realloc(*text_alarm, sizeof(char)*(len + 1));
            snprintf(*text_alarm, len + 1, "notify-send \"Reminder\" \"%s today at %02d:%02d.\"", alarm.event->summary, alarm.event->start.hour, alarm.event->start.minute);
            break;
        case 1:
            len = snprintf(NULL, 0, "notify-send \"Reminder\" \"%s tomorrow at %02d:%02d.\"", alarm.event->summary, alarm.event->start.hour, alarm.event->start.minute);
            *text_alarm = realloc(*text_alarm, sizeof(char)*(len + 1));
            snprintf(*text_alarm, len + 1, "notify-send \"Reminder\" \"%s tomorrow at %02d:%02d.\"", alarm.event->summary, alarm.event->start.hour, alarm.event->start.minute);
            break;
        default:
            len = snprintf(NULL, 0, "notify-send \"Reminder\" \"%s in %d days.\"", alarm.event->summary, diff_in_days);
            *text_alarm = realloc(*text_alarm, sizeof(char)*(len + 1));
            snprintf(*text_alarm, len + 1, "notify-send \"Reminder\" \"%s in %d days.\"", alarm.event->summary, diff_in_days);
            break;
    }
}

void cleanup(struct events_array *events, struct alarms_array *alarms, WINDOW **windows, const unsigned int n)
{
    /*
     * Frees and unsets events_array and alarms_array before exiting.
     * This function also clears the windows and ends ncurses.
     *
     * Parameters:
     * - events: events_array to be freed and nulled
     * - alarms: alarms_array to be nulled.
     *           There no need to free because the array contains pointers.
     * - windows: array with windows to clean
     * - n: number of active windows
     */

    unsigned int i = 0;
    free(alarms->alarms);
    alarms->alarms = NULL;
    for (i = 0; i < events->n; i++)
    {
        free(events->events[i].filename);
        events->events[i].filename = NULL;
        free(events->events[i].summary);
        events->events[i].summary = NULL;
    }
    free(events->events);
    events->events = NULL;
    clear_windows(windows, n);
    endwin();
}

void update_events_to_arrays(const struct calendar calendars[], const unsigned int n_calendars, struct events_array *e, struct alarms_array *a, const unsigned int reload)
{
    unsigned int i = 0;
    unsigned int n_events = 0;
    unsigned int n_alarms = 0;

    if (reload == 1)
    {
        /* Stop ncurses, inform the user about the update and update the data.
         * Also, we should stop the alarm because it is possible that one timing out earlier has been loaded.
         */
        alarm(0);
        endwin();
        const time_t t = time(NULL);
        const struct tm tm = *localtime(&t);
        printf("%d-%02d-%02d %02d:%02d:%02d - Updating calendars\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
        FILE *file = popen("vdirsyncer sync", "r");
        pclose(file);

        /* Free and null events and alarms */
        free(a->alarms);
        a->alarms = NULL;
        a->n = 0;
        for (i = 0; i < e->n; i++)
        {
            free(e->events[i].filename);
            e->events[i].filename = NULL;
            free(e->events[i].summary);
            e->events[i].summary = NULL;
        }
        free(e->events);
        e->events = NULL;
        e->n = 0;
    }

    for (i = 0; i < n_calendars; i++)
    {
        unsigned int *tmp = count_events(calendars[i].path);
        n_events += tmp[0];
        n_alarms += tmp[1];
    }

    e->events = malloc(sizeof(struct event)*n_events);
    a->alarms = malloc(sizeof(struct alarm)*n_alarms);

    for (i = 0; i < n_calendars; i++)
    {
        load_events(calendars[i].path, e, a, i);
    }

    /* Sort the alarms by unix timestamp */
    if (a->n > 0)
    {
        qsort(a->alarms, a->n, sizeof(struct alarm), compare_unix_times);
    }
}


int main() {

    unsigned int i = 0;

    const time_t t = time(NULL);
    const struct tm tm = *localtime(&t);
    unsigned int day = tm.tm_mday;
    unsigned int month = tm.tm_mon + 1;
    unsigned int year = tm.tm_year + 1900;

    int c = 0;

    unsigned int d = 0;
    unsigned int sel = 0;
    unsigned int exit = 0;
    unsigned int view_day_active;
    int action = -99;
    unsigned int skip_getch = 0;
    unsigned int bool_display_day = 0;

    /* Events */
    struct events_array events;
    events.n = 0;
    events.events = NULL;

    /* Alarms */
    struct alarms_array alarms;
    alarms.n = 0;
    alarms.alarms = NULL;

    view_day_active = 0;

    /* Update data to arrays */
    unsigned int n_calendars = sizeof(calendars)/sizeof(calendars[0]);
    update_events_to_arrays(calendars, n_calendars, &events, &alarms, 0);   /* 0 means no need to reload the data
                                                                             We are filling it for the first time
                                                                             */
    set_alarm = alarms.n > 0 ? 1 : 0;

    /* Setup signals */
    signal(SIGALRM, sig_handler);
    unsigned int current_alarm = 0;
    char *text_alarm = NULL;

    /* Start ncurses */
    setup_ncurses(rgb_colors, n_colors);

    WINDOW *windows[MAX_WINDOWS];
    WINDOW *view_day_win[1];

    refresh();
    view_day_win[0] = NULL;
    unsigned int active_windows = print_cal(stdscr, windows, year, month, app, events, sel);
    while(exit == 0)
    {
        /* Alarms */
        if (notify_alarm == 1)
        {
            format_text_alarm(&text_alarm, alarms.alarms[current_alarm]);
            printf("%s", text_alarm);
            FILE *file = popen(text_alarm, "r");
            pclose(file);
            notify_alarm = 0;
            current_alarm++;
        }
        if (set_alarm == 1)
        {
            current_alarm = setup_alarm(alarms);
            set_alarm = 0;
        }

        if (skip_getch == 0)
        {
            c = getch();
            action = get_action(c, keys, len_keys);
        }
        else
        {
            skip_getch = 0;
        }
        if (action == -1)
        {
            continue;
        }
        if (action == reload_data)
        {
            update_events_to_arrays(calendars, n_calendars, &events, &alarms, 1); /* 1 means reloading the data */
        }

        if (view_day_active == 0)
        {
            d = days_in_view(year, month);
            switch(action)
            {
                case go_today:
                    month = tm.tm_mon + 1;
                    year = tm.tm_year + 1900;
                    sel = square_day(d, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
                    break;
                case next_month:
                    increase_month(&year, &month);
                    sel = 0;
                    break;
                case previous_month:
                    decrease_month(&year, &month);
                    break;
                case up:
                case down:
                case left:
                case right:
                    move_sel(action, &year, &month, &day, &sel, d);
                    break;
                case disp_view_day:
                    view_day_active = 1;
                    action = -2;
                    bool_display_day = 1;
                    break;
                case quit:
                    exit = 1;
                    break;
            }
            clear_windows(windows, active_windows);
            active_windows = print_cal(stdscr, windows, year, month, app, events, sel);
        }
        if (view_day_active == 1)
        {
            if (bool_display_day == 1)
            {
                view_day(stdscr, view_day_win, year, month, day, sel, events, app);
                bool_display_day = 0;
            }
            switch(action)
            {
                case disp_view_day:
                    view_day_active = 0;
                    skip_getch = 1;
                    action = -2;
                    clear_windows(view_day_win, 1);
                    refresh();
                    continue;
                case quit:
                    exit = 1;
                    break;
            }
        }
    }
    cleanup(&events, &alarms, windows, active_windows);
    return(0);
}
